== Structural ==

A Drupal module that aims to improve the relationship between nodes and menu
links, making it easier to manage your site's structure.